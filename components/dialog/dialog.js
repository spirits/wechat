// components/dialog/dialog.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isHidden:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    touchmove(){},
    closeDialog:function(){
      this.triggerEvent("showCheck",{flag:true});
    }
  }
})
