//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    kw:"",
    bannerInfo:{
      banner_list:[
        {
          "path": "/images/1.jpg",
          'title': "one"
        },
        {
          "path": "/images/2.jpg",
          'title': "two"
        },
        {
          "path": "/images/3.jpg",
          'title': "three"
        },
        {
          "path": "/images/4.jpg",
          'title': "four"
        },
        {
          "path": "/images/5.jpg",
          'title': "five"
        }
      ]
    },
    selected:[
      {
        id:1,
        path:"/images/1_01.png",
        title:"找投顾"
      },
      {
        id:2,
        path: "http://dev.zhangtingtv.com/wx/7ce260fcb4a212bad2309a57ce35751a.png",
        title: "涨听课堂"
      },
      {
        id:3,
        path: "/images/1_03.png",
        title: "财说"
      },
      {
        id:4,
        path: "/images/1_04.png",
        title: "涨听会员"
      }
    ],
    isHidden:true
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (!app.globalData.userInfo) {
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
        }
      })
    }
  },
  searchsubmit:function(e){
    if(!this.data.kw){
      wx.showToast({
        title: '请输入搜索内容',
        icon:"none"
      })
    }else{
      //发送请求
    }
  },
  category_click:function(e){
    var id = e.currentTarget.dataset.id;
    console.log(id);
    //页面请求或者跳转
  },
  onShowCheck:function(e){
   var flag = e.detail.flag;
    this.setData({
      isHidden:flag
    });
  }
 
 
})
