// components/banner/banner.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    banner_list:{
      type:Array,
      value: []
    },
    auto:{
      type: Boolean,
      value:true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    ontouchend:function(){
      this.properties.auto = true;
    },
    ontouchstart:function(){
      this.properties.auto = false;
    }
  }
})
