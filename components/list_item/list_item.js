// components/list_item/list_item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },
  audioCtx:null,

  /**
   * 组件的初始数据
   */
  data: {
    items:[
      {
        id:1,
        content:"编故事压法忘恩负义第十放映室地方税法一个，伴随广发二月份搭顺风车雇佣工人房丢失发个IE我是法第三方，打不死阿凤阿月份打包和v个啊更丰富覆盖",
        audio:"http://96.f.1ting.com/5aebd69a/11d7ef45a9b7b705632d638d4668fd3f/znew1/200408/12a/7.mp3",
        listen:272,
        avatar:"http://ot8a1i72r.bkt.clouddn.com/avatar/20180222172448a43840f4fa8fd6af55bb0fa2b5f6e9dd8471.?imageView2/1/w/100/h/100/q/75|imageslim",
        author:"peen66",
        created:"01-55",
        author_id:10,
        free:true,
        playing:false,
        pause:false,
        status:"开始播放"
      },
      {
        id: 2,
        content: "编故事压法忘恩负义第十放映室地方税法一个，伴随广发二月份搭顺风车雇佣工人房丢失发个IE我是法第三方，打不死阿凤阿月份打包和v个啊更丰富覆盖",
        audio: "http://96.f.1ting.com/5aebd832/cba3a42bab28bcb895039c29c8ba270d/i/1031/7/14.mp3",
        listen: 272,
        avatar: "http://ot8a1i72r.bkt.clouddn.com/avatar/20180222172448a43840f4fa8fd6af55bb0fa2b5f6e9dd8471.?imageView2/1/w/100/h/100/q/75|imageslim",
        author: "peen66",
        created: "01-55",
        author_id: 10,
        free: false,
        playing: false,
        pause: false,
        status: "开始播放"
      },
      {
        id: 3,
        content: "编故事压法忘恩负义第十放映室地方税法一个，伴随广发二月份搭顺风车雇佣工人房丢失发个IE我是法第三方，打不死阿凤阿月份打包和v个啊更丰富覆盖",
        audio: "http://96.f.1ting.com/5aebd832/cba3a42bab28bcb895039c29c8ba270d/i/1031/7/14.mp3",
        listen: 272,
        avatar: "http://ot8a1i72r.bkt.clouddn.com/avatar/20180222172448a43840f4fa8fd6af55bb0fa2b5f6e9dd8471.?imageView2/1/w/100/h/100/q/75|imageslim",
        author: "peen66",
        created: "01-55",
        author_id: 10,
        free: true,
        playing: false,
        pause: false,
        status: "开始播放"
      },
      {
        id: 4,
        content: "编故事压法忘恩负义第十放映室地方税法一个，伴随广发二月份搭顺风车雇佣工人房丢失发个IE我是法第三方，打不死阿凤阿月份打包和v个啊更丰富覆盖",
        audio: "http://96.f.1ting.com/5aebd832/cba3a42bab28bcb895039c29c8ba270d/i/1031/7/14.mp3",
        listen: 272,
        avatar: "http://ot8a1i72r.bkt.clouddn.com/avatar/20180222172448a43840f4fa8fd6af55bb0fa2b5f6e9dd8471.?imageView2/1/w/100/h/100/q/75|imageslim",
        author: "peen66",
        created: "01-55",
        author_id: 10,
        free: false,
        playing: false,
        pause: false,
        status: "开始播放"
      },
      {
        id: 5,
        content: "编故事压法忘恩负义第十放映室地方税法一个，伴随广发二月份搭顺风车雇佣工人房丢失发个IE我是法第三方，打不死阿凤阿月份打包和v个啊更丰富覆盖",
        audio: "http://96.f.1ting.com/5aebd832/cba3a42bab28bcb895039c29c8ba270d/i/1031/7/14.mp3",
        listen: 272,
        avatar: "http://ot8a1i72r.bkt.clouddn.com/avatar/20180222172448a43840f4fa8fd6af55bb0fa2b5f6e9dd8471.?imageView2/1/w/100/h/100/q/75|imageslim",
        author: "peen66",
        created: "01-55",
        author_id: 10,
        free: false,
        playing: false,
        pause: false,
        status: "开始播放"
      }
    ],
  },
  attached:function(){
    this.audioCtx = wx.createAudioContext("item-audio",this);
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onListenMusic:function(e){
      var free = e.currentTarget.dataset.free;
      if(free){
        var id = e.currentTarget.dataset.id;
        this.getItem(id);
      }else{
        this.triggerEvent("showCheck", { flag: false });
        this.setReset();
      }
    },
    endPlay:function(){
      getItem(0);
    },
    getItem:function(id){
      if(this.data.items){
        for(var i=0;i< this.data.items.length;i++){
          if(this.data.items[i].id == id){
            if (this.data.items[i].playing){
              this.data.items[i].playing = false;
              this.data.items[i].pause = true;
              this.data.items[i].status = "暂停播放";
              this.audioCtx.pause();
            }else{
              if (!this.data.items[i].pause){
                this.audioCtx.setSrc(this.data.items[i].audio);
              }
              this.audioCtx.play();
              this.data.items[i].playing = true;
              this.data.items[i].pause = false;
              this.data.items[i].status = "正在播放";
            }
          }else{
            this.data.items[i].playing = false;
            this.data.items[i].pause = false;
            this.data.items[i].status = "开始播放";
          }
        }
        this.setData({
          items: this.data.items
        });
      }
    },
    loadingData:function(){
      var data = this.data.items.concat(this.data.items);
      this.setData({
        items: data
      });
    },
    setReset:function(){
      if (this.data.items) {
        for (var i = 0; i < this.data.items.length; i++) {
            this.data.items[i].playing = false;
            this.data.items[i].pause = false;
            this.data.items[i].status = "开始播放";
        }
        this.audioCtx.pause();
        this.audioCtx.seek(0);
        this.setData({
          items: this.data.items
        });
      }
    }
  }
})
